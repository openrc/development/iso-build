#!/bin/bash
find ./ -exec sed -i 's/artix/openstage/g' {} \;
find ./ -exec sed -i 's/artools/opentools/g' {} \;
find ./ -exec sed -i 's/Artix/OpenStage/g' {} \;
find ./ -exec sed -i 's/ARTIX/OPENSTAGE/g' {} \;
#find ./ -exec sed -i 's/Artix linux/OpenStage linux/g' {} \;
#find ./ -exec sed -i 's/artixlinux/openstagelinux/g' {} \;
#find ./ -exec sed -i 's/Artix Linux/OpenStage Linux/g' {} \;
